;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

(ns to.tuo.b1ng0.model)

(def title "BINGO")
(def column-range 15)

(defn annotate-history [history]
  (reduce (fn [annotated-history cell]
            (conj annotated-history
                  {:cell  cell
                   :prior (count (filter
                                  #(= (:cell %) cell)
                                  annotated-history))}))
          []
          history))

(defn make-master-card [title column-range]
  (let [column-count (count title)]
    (mapv (fn [i] (vec (range (inc (* i column-range))
                             (inc (* (inc i) column-range)))))
          (range column-count))))

(defn make-user-card [master-card]
  (mapv #(->> % shuffle (take 5) vec) master-card))

(defn initial-callable-cells [title column-range]
  (set
   (map inc (range (* (count title) column-range)))))

(defn transpose [m]
  (apply mapv vector m))

(defn cell->title-character
  [{:as _state :keys [column-range title]} cell]
  ;; TODO:
  ;; get column header character from cell without assuming
  ;;  cell value is a number which can be mathed
  (let [character-index (quot (dec cell) column-range)]
    (nth title character-index)))

(defn toggle [collection element]
  ((if (some #(= element %) collection)
     disj conj)
   collection
   element))

(defn remove-call-from-users [state cell]
  (reduce (fn [state user]
            (update-in state [:users user :called-cells] disj cell))
          state
          (-> state :users keys)))

(defn toggle-cell [state cell]
  (-> state
      (remove-call-from-users        cell)
      (update :call-history   conj   cell)
      (update :callable-cells toggle cell)
      (update :called-cells   toggle cell)))

(defn toggle-cell! [state cell]
  (swap! state toggle-cell cell))

(defn call-cell [state cell]
  (-> state
      (update :call-history   conj cell)
      (update :callable-cells disj cell)
      (update :called-cells   conj cell)))

(defn user-call-cell [state {:keys [user cell]}]
  (if (contains? (:called-cells state) cell)
    (update-in state [:users user :called-cells] conj cell)
    state))

(defn player-only-call-cell [state cell]
  (update-in state [:player-only :called-cells] toggle cell))

(defn user-call-cell! [state user-and-cell]
  (swap! state user-call-cell user-and-cell))

(defn random-callable-cell [{:keys [callable-cells]}]
  (rand-nth (into [] callable-cells)))

(defn call-random-cell [{:keys [callable-cells] :as state}]
  (if (empty? callable-cells)
    state
    (let [random-cell (random-callable-cell state)]
      (call-cell state random-cell))))

(defn call-random-cell! [state]
  (swap! state call-random-cell))

(defn get-most-recent-called-cell [state]
  (let [{:keys [called-cells call-history]} @state]
    (last (filter #(contains? called-cells %) call-history))))

(defn called-cell? [{:keys [called-cells]} cell]
  (some #(= cell %) called-cells))

(defn diagonals [card]
  (transpose
   (let [dimension (count card)]
     (for [index (range dimension)]
       [(nth (nth card index) index)
        (nth (nth card (- dimension index 1)) index)]))))

(defn user-called? [state cell]
  (if (= "player card only" (:play-type state))
    (contains?
     (get-in state [:player-only :called-cells])
     cell)
    (contains?
     (get-in state [:users (:user state) :called-cells])
     cell)))

(defn clear-user-called-cells [game user]
  (update-in game [:users user :called-cells] empty))

(defn clear-all-users-called-cells [game]
  (reduce clear-user-called-cells game (-> game :users keys)))

(defn clear-call-history [game]
  (-> game
      (update :call-history empty)
      (assoc
       :callable-cells (initial-callable-cells title column-range))
      (update :called-cells empty)))

(defn new-game [game]
  (-> game
      clear-call-history
      clear-all-users-called-cells))

(defn create-state []
  (let [master-card (make-master-card title column-range)
        callable-cells (initial-callable-cells title column-range)]
    {:title          title
     :column-range   column-range
     :callable-cells callable-cells
     :master-card    master-card
     :users          {}
     :call-history   []
     :called-cells   #{}}))

(defn create-state-with-player-only []
  (let [master-card (make-master-card title column-range)]
    (assoc (create-state) :player-only
           {:called-cells #{}
            :card         (make-user-card master-card)})))

(defn create-state! []
  (atom (create-state)))

(defn new-game! [state]
  (swap! state new-game))

(defn make-user [state user]
  {:user         user
   :card         (make-user-card (:master-card @state))
   :called-cells #{}})

(defn request-user! [state user]
  (if (get-in @state [:users user])
    @state
    (swap! state assoc-in [:users user] (make-user state user))))

(defn logout [state user]
  (-> state
      (dissoc :user)
      (update :users dissoc user)))

(defn request-logout! [state user]
  (swap! state #(logout % user)))

;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with b1ng0.  If not, see <http://www.gnu.org/licenses/>.
