;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

(ns to.tuo.b1ng0.server
  (:gen-class)
  (:require
   [clojure.pprint :refer [pprint]]
   ;; TODO: use command line parsing
   ;; [clojure.tools.cli :refer [parse-opts]]
   [compojure.core :refer [defroutes GET POST]]
   [to.tuo.b1ng0.model :as model]
   [hiccup.page :refer [html5 include-css include-js]]
   [org.httpkit.server :refer [run-server]]
   [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
   [ring.middleware.reload :refer [wrap-reload]]
   [taoensso.sente :refer [make-channel-socket! start-server-chsk-router!]]
   [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]))

(def dev? true)
(defn debug [debug? x]
  (when debug?
    (pprint x)
    (flush))
  x)

(def state
  (model/create-state!))

(defonce channel-socket
  (make-channel-socket! (get-sch-adapter)))

;; define socket helper functions
(let [{:keys [ch-recv send-fn connected-uids
              ajax-post-fn ajax-get-or-ws-handshake-fn]}
      channel-socket]
  (def ring-ajax-post                ajax-post-fn)

  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)

  ;; ChannelSocket's receive channel
  (def ch-chsk                       ch-recv)

  ;; ChannelSocket's send API fn
  (def chsk-send!                    send-fn)

  ;; Watchable, read-only atom
  (def connected-uids                connected-uids))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn handle-home [http-request]
  (html5
   {:lang "en"}
   [:head
    [:meta {:charset "UTF-8"}]
    [:meta {:name    "viewport"
            :content "width=device-width, initial-scale=1"}]
    (include-css "b1ng0.css")
    [:title "BINGO"]]
   [:body
    [:div#sente-csrf-token
     {:data-csrf-token (:anti-forgery-token http-request)}]
    [:div#app [:h1 "loading..."]]
    ;; TODO: use command line or env to determine which js to load
    #_(comment
        (include-js "main.js")
        (include-js (if dev?
                      "/cljs-out/dev-main.js"
                      "main.js")))
    (include-js (if dev?
                  "/cljs-out/dev-main.js"
                  "main.js"))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn broadcast-game-message [state]
  [:b1ng0/state @state])

(defn broadcast-game! []
  (chsk-send!
   :taoensso.sente/nil-uid
   (broadcast-game-message state)))

(defn ping-response [event-message]
  (debug false event-message)
  ".")

(defn handle-chsk+ws-ping [_event-message]
  ;; uncomment to see "." for every ping
  #_(print (ping-response _event-message))
  #_(flush))

(defn handle-chsk+uidport-open [_event-message]
  (broadcast-game!))

(defn handle-b1ng0+request-random-cell [_event-message]
  (model/call-random-cell! state)
  (broadcast-game!))

(defn handle-b1ng0+fetch-state [_event-message]
  (broadcast-game!))

(defn handle-b1ng0+user-call-cell [{:as _event-message :keys [?data]}]
  (model/user-call-cell! state ?data)
  (broadcast-game!))

(defn handle-b1ng0+request-new-game [_event-message]
  (model/new-game! state)
  (broadcast-game!))

(defn handle-b1ng0+request-user [{:as _event-message :keys [?data]}]
  (model/request-user! state ?data)
  (broadcast-game!))

(defn handle-b1ng0+request-logout [{:as _event-message :keys [?data]}]
  (model/request-logout! state ?data)
  (broadcast-game!))

(defn handle-b1ng0+toggle-cell! [{:as _event-message :keys [?data]}]
  (model/toggle-cell! state ?data)
  (broadcast-game!))

(defn ws-event-id->handler-name [id]
  (str "handle-" (namespace id) "+" (name id)))

(defn ws-event-id-handled? [id]
  (#{:chsk/ws-ping
     :chsk/uidport-open

     :b1ng0/request-random-cell
     :b1ng0/user-call-cell
     :b1ng0/toggle-cell!
     :b1ng0/fetch-state
     :b1ng0/request-new-game
     :b1ng0/request-user
     :b1ng0/request-logout}
   id))

(defn handler-name->handler
  [handler-name]
  (->> handler-name
       symbol
       (ns-resolve (-> #'handler-name->handler meta :ns))))

(defn handle-unknown-event-message [event-message]
  (debug true ["handle-unknown-event-message" event-message]))

(defn event-message-handler
  [{:as event-message :keys [id]}]
  (let [handler
        (if (ws-event-id-handled? id)
          (-> id ws-event-id->handler-name handler-name->handler)
          handle-unknown-event-message)]
    (handler event-message)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defroutes http-routes
  (GET "/" http-request (handle-home http-request))
  (GET "/chsk" http-request
    (ring-ajax-get-or-ws-handshake http-request))
  (POST "/chsk" http-request
    (ring-ajax-post http-request)))

(def http-router
  (wrap-defaults http-routes site-defaults))

(defonce ?stop-http-server! (atom nil))

(defn stop-http-server! []
  (when (fn? ?stop-http-server!)
    (@?stop-http-server! :timeout 100)
    (reset! ?stop-http-server! nil)))

(defn start-http-server! [& [port]]
  (stop-http-server!)
  (let [port                 (if dev? (or port 0) 8081)
        top-handler          (if dev?
                               (wrap-reload (var http-router))
                               http-router)
        http-server-stop-fn! (run-server top-handler {:port port})
        port                 (:local-port (meta http-server-stop-fn!))
        uri                  (str "http://localhost:" port)]
    (when dev?
      (try
        (.browse (java.awt.Desktop/getDesktop) (java.net.URI. uri))
        (catch java.awt.HeadlessException _)))
    (println (str "http server running at " uri))
    (reset! ?stop-http-server! http-server-stop-fn!)))

(defonce ?stop-websocket-router! (atom nil))

(defn stop-websocket-router! []
  (when (fn? @?stop-websocket-router!)
    (@?stop-websocket-router!)
    (reset! ?stop-websocket-router! nil)))

(defn start-websocket-router! []
  (stop-websocket-router!)
  (reset! ?stop-websocket-router!
    (start-server-chsk-router! ch-chsk #'event-message-handler)))

(defn stop! []
  (stop-http-server!)
  (stop-websocket-router!))

(defn start! []
  (start-http-server!)
  (start-websocket-router!))

(def cli-options
  [["-p" "--port PORT" "port number"
    :default 0
    :parse-fn #(Integer/parseInt %)
    :validate [#(<= 0 % 0xFFFF)
               "must be a number between 0 and 65536"]]
   ["-v" "--verbose" "verbosity level"
    :id :verbosity
    :default 0
    :update-fn inc]
   ["-h" "--help"]])

(defn -main [& _args]
  ;; TODO: get to command line parsing
  ;; (let [{:keys [port verbosity errors arguments]}
  ;;       (parse-opts args cli-options)]
  ;;   (if errors
  ;;     (pprint errors)))
  (start!))

;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.

;; You should have received a copy of the GNU Affero General Public
;; License along with b1ng0. If not, see <http://www.gnu.org/licenses/>.
