;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

(ns ^:figwheel-hooks to.tuo.b1ng0.web-ui
  (:refer-clojure :exclude [atom])
  (:require
   [clojure.pprint :refer [pprint]]
   [to.tuo.b1ng0.model :as model]
   [reagent.core :refer [atom]]
   [reagent.dom :refer [render]]
   [taoensso.sente
    :refer [make-channel-socket-client!
            start-client-chsk-router!]]))

(enable-console-print!)

(defn get-app-element []
  (js/document.getElementById "app"))

(def app
  (atom (get-app-element)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; websocket setup

(def ?csrf-token
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

(defonce channel-socket-client
  (when ?csrf-token
    (make-channel-socket-client! "/chsk" ?csrf-token {:type :auto})))

(let [{:keys [chsk ch-recv send-fn state]}
      channel-socket-client]

  (def chsk       chsk)

  ;; ChannelSocket's receive channel
  (def ch-chsk    ch-recv)

  ;; ChannelSocket's send API fn
  (def chsk-send! send-fn)

  ;; Watchable, read-only atom
  (def chsk-state state))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; startup

(defonce state
  (atom (model/create-state-with-player-only)))

(defn add-on-click [reagent-component f]
  (let [on-click {:on-click f}]
    (if (= 1 (count reagent-component))
      (conj reagent-component on-click)
      (if (map? (reagent-component 1))
        (let [[first attributes & rest] reagent-component]
          (vec (apply list first (merge attributes on-click) rest)))
        (let [[first & rest] reagent-component]
          (vec (apply list first on-click rest)))))))

(defn switch-to-player-and-master [state]
  (assoc state :play-type "player and master"))

(defn switch-to-player-and-master! [state]
  (swap! state switch-to-player-and-master))

(defn switch-to-player-and-master-button-static [_state]
  [:button "switch to player and master card"])

(defn switch-to-player-and-master-button [state]
  (-> state
      switch-to-player-and-master-button-static
      (add-on-click
       #(switch-to-player-and-master! state))))

(defn switch-to-player-only [state]
  (assoc state :play-type "player card only"))

(defn switch-to-player-only! [state]
  (swap! state switch-to-player-only))

(defn switch-to-player-card-only-button-static [_state]
  [:button "switch to player card only"])

(defn switch-to-player-card-only-button [state]
  (-> state
      switch-to-player-card-only-button-static
      (add-on-click
       #(switch-to-player-only! state))))

(defn play-type-section [state]
  [:div.play-type-section
   (if (= "player card only" (:play-type @state))
     [switch-to-player-and-master-button state]
     [switch-to-player-card-only-button state])])

(defn user-request-message [requested-user]
  [:b1ng0/request-user requested-user])

(defn send-user-request! [requested-user]
  (chsk-send! (user-request-message requested-user)))

(defn wait-on-user [state user]
  (assoc state :waiting-on-user user))

(defn request-user! [state requested-user]
  (swap! state wait-on-user requested-user)
  (send-user-request! requested-user))

(defn requested-user-textfield [_state]
  [:input#user
   {:type        :text
    :placeholder "user"}])

(defn request-user-button-static [_state]
  [:button#request-user-button "login"])

(defn request-user-button [state]
  (-> state
      request-user-button-static
      (add-on-click
       #(let [requested-user (.-value
                              (js/document.getElementById
                               "user"))]
          (request-user! state requested-user)))))

(defn login-section [state]
  [:div#login
   [requested-user-textfield state]
   [request-user-button state]])

(def logout-button-static [:div#logout [:button "logout"]])

(defn request-logout! [user]
  (chsk-send! [:b1ng0/request-logout user]))

(defn logout! [state]
  (let [user (:user @state)]
    (request-logout! user)
    (swap! state dissoc :user)))

(defn display-logged-in-user-section [state user]
  [:div#logged-in-user
   [:h1 user]
   (add-on-click
    logout-button-static
    #(logout! state))])

(defn user-section [state]
  (let [user (:user @state)]
    [:div#user-section
     (if user
       [display-logged-in-user-section state user]
       [login-section state])]))

(defn request-new-game! [_state]
  (chsk-send! [:b1ng0/request-new-game]))

(defn new-game-button-static [_state]
  [:button "new game"])

(defn new-game-button [state]
  (-> state
      new-game-button-static
      (add-on-click
       #(request-new-game! state))))

(defn cell-with-title-character [state cell]
  [:span.cell-with-title-character
   [:span.title-character
    (model/cell->title-character @state cell)]
   " "
   [:span.cell cell]])

(defn history-cell-with-title-character [state cell]
  [:span.cell-with-title-character
   {:class (if (even? (:prior cell))
             "called"
             "not-called")}
   [:span.title-character
    (model/cell->title-character @state (:cell cell))]
   " "
   [:span.cell (:cell cell)]])

(defn call-history [state]
  (let [call-history (:call-history @state)]
    (when-not (empty? call-history)
      [:div
       [:h1 "history"]
       [:div.cell-list.history
        (for [called-cell
              (reverse (model/annotate-history call-history))]
          ^{:key called-cell}
          [history-cell-with-title-character
           state
           called-cell])]])))

(defn fetch-state! []
  (chsk-send! [:b1ng0/fetch-state]))

(defn request-random-cell! []
  (chsk-send! [:b1ng0/request-random-cell]))

(defn call-random-cell [state]
  [:div.call-random-cell
   [:button {:on-click request-random-cell!} "call new number"]
   (let [most-recent-called-cell
         (model/get-most-recent-called-cell state)]
     (when most-recent-called-cell
       [cell-with-title-character
        state
        most-recent-called-cell]))])

(defn player-only-call-cell! [state cell]
  (swap! state model/player-only-call-cell cell))

(defn user-call-cell! [state cell]
  (if (= "player card only" (:play-type @state))
    (player-only-call-cell! state cell)
    (chsk-send!
     [:b1ng0/user-call-cell {:cell cell
                             :user (:user @state)}])))

(defn user-cell [state cell]
  [:div.user-row
   {:class    (list
               (if (model/user-called? @state cell)
                 "user-called"
                 "not-user-called"))
    :on-click #(user-call-cell! state cell)}
   cell])

(defn user-column [state title-char column]
  [:div.user-column
   (apply list
          ^{:key title-char}
          [:div.title-char title-char]
          (for [cell column]
            ^{:key cell}
            [user-cell state cell]))])

(defn create-player-only-card-data [state]
  (assoc
   state
   :player-only-card
   (model/make-user-card (:master-card state))))

(defn create-player-only-card-data! [state]
  (swap! state create-player-only-card-data))

(defn get-player-only-card-data [state]
  (get-in @state [:player-only :card]))

(defn get-user-card-data [state]
  (if (= "player card only" (:play-type @state))
    (get-player-only-card-data state)
    (let [{:as state :keys [user]} @state]
      (get-in state [:users user :card]))))

(defn user-card [state]
  [:div.user-card
   (let [card-data (get-user-card-data state)]
     (for [[title-char column] (map vector
                                    (:title @state)
                                    card-data)]
       ^{:key column}
       [user-column state title-char column]))])

(defn user-card-section [state]
  (when-let [user (:user @state)]
    [:div.user-card-section
     [:h1
      {:class (when (and (:waiting-on-user @state)
                         (:user @state))
                "waiting-on-user")}
      "player card"]
     [user-card state]]))

(defn master-cell [state cell]
  [:div.master-row
   {:class    (if (model/called-cell? @state cell)
                "called"
                "not-called")
    :on-click #(chsk-send! [:b1ng0/toggle-cell! cell])}
   cell])

(defn master-column [state title-char column]
  [:div.master-column
   (apply list
          ^{:key title-char}
          [:div.title-char title-char]
          (for [cell column]
            ^{:key cell}
            [master-cell state cell]))])

(defn master-card [state]
  [:div.master-card
   (for [[title-char column] (map vector
                                  (:title @state)
                                  (:master-card @state))]
     ^{:key column}
     [master-column state title-char column])])

(defn master-card-section [state]
  [:div.master-card-section
   [:h1 "master card"]
   [master-card state]])

(defn current-users [users]
  [:div.current-user-list
   (for [user users]
     ^{:key user}
     [:span.user user])])

(defn current-users-section [state]
  (when-let [users (-> @state :users keys)]
    [:div.current-users-section
     [:h1 "current users"]
     [current-users users]]))

(defn player-and-master-section [state]
  [:div
   [user-section state]
   [call-random-cell state]
   [user-card-section state]
   [call-history state]
   [current-users-section state]
   [master-card-section state]
   [new-game-button state]])

(defn player-card-only-section [state]
  [:div.player-card-only-section
   [:h1 "player card only"]
   [user-card state]])

(defn page [state]
  [:div.page
   [play-type-section state]
   (if (= "player card only" (:play-type @state))
     [player-card-only-section state]
     [player-and-master-section state])])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; websocket routing

(defn error-message
  [header event-message]
  (str header "\n" (-> event-message pprint with-out-str)))

(defn ping-response [_event-message]
  ".")

(defn handle-chsk-recv-chsk-ws-ping [event-message]
  (comment
    (print (ping-response event-message))
    (flush)))

(defn handle-chsk-handshake [event-message]
  (println (error-message "handle chsk handshake" event-message))
  (fetch-state!))

(defn handle-chsk-recv-b1ng0-unknown
  [event-message]
  (println
   (error-message "unknown chsk recv b1ng0 message" event-message)))

(defn ?arriving-user [state]
  (if-let [user (:waiting-on-user state)]
    (get (into #{} (keys (:users state))) user)))

(defn update-waiting-on-user [state]
  (if-let [new-user (?arriving-user state)]
    (-> state
        (assoc :user new-user)
        (dissoc :waiting-on-user))
    state))

(defn merge-state-from-server [state new-state]
  (-> state
      (merge new-state)
      update-waiting-on-user))

(defn merge-state-from-server! [state new-state]
  (swap! state merge-state-from-server new-state))

(defn handle-chsk-recv-b1ng0-state [{:as _event-message :keys [?data]}]
  (merge-state-from-server!
   state (:b1ng0/state (into {} [?data]))))

(defn handle-unknown-chsk-message [event-message]
  (println (error-message "unknown chsk message" event-message)))

(defn handle-unknown-chsk-recv-message [event-message]
  (println (error-message "unknown chsk recv message" event-message)))

(defn handle-chsk-recv-chsk-unknown [event-message]
  (println
   (error-message "unknown chsk recv chsk message" event-message)))

(defn handle-chsk-recv-chsk
  [{:as event-message :keys [?data]}]
  ((condp = (-> ?data first name keyword)
     :ws-ping handle-chsk-recv-chsk-ws-ping

     handle-chsk-recv-chsk-unknown)
   event-message))

(defn handle-chsk-recv-b1ng0
  [{:as event-message :keys [?data]}]
  ((condp = (-> ?data first name keyword)
     :state handle-chsk-recv-b1ng0-state

     handle-chsk-recv-b1ng0-unknown)
   event-message))

(defn handle-chsk-recv
  [{:as event-message :keys [?data]}]
  ((condp = (-> ?data first namespace keyword)
     :chsk  handle-chsk-recv-chsk
     :b1ng0 handle-chsk-recv-b1ng0

     handle-unknown-chsk-recv-message)
   event-message))

(defn handle-chsk-state [{:as _event-message :keys [?data]}]
  (println (error-message "handle chsk state ?data" ?data)))

(defn handle-chsk-message
  [{:as event-message :keys [id]}]
  ((condp = (-> id name keyword)
     :recv      handle-chsk-recv
     :handshake handle-chsk-handshake
     :state     handle-chsk-state

     handle-unknown-chsk-message)
   event-message))

(defn handle-unknown-namespace [event-message]
  (println (error-message "unknown namespace" event-message)))

(defn event-message-handler [{:as event-message :keys [id]}]
  ((condp = (-> id namespace keyword)
     :chsk handle-chsk-message

     handle-unknown-namespace)
   event-message))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; websocket router

(defonce ?stop-router! (atom nil))
(defn stop-router! []
  (when (fn? ?stop-router!) (?stop-router!))
  (reset! ?stop-router! nil))
(defn start-router! []
  (stop-router!)
  (reset!
   ?stop-router!
   (start-client-chsk-router!
    ch-chsk event-message-handler)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; start things off

(defn mount [state app]
  (render [page state] app))

(defn ^:after-load re-render []
  (reset! app (get-app-element))
  (when @app
    (mount state @app)))

(defonce start-up
  (do
    (when @app
      (start-router!)
      (mount state @app))
    true))

(comment
  @state
  (re-render))

;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with b1ng0.  If not, see <http://www.gnu.org/licenses/>.
