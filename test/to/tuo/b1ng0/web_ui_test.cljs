;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

;; http://localhost:9500/figwheel-extra-main/auto-testing

(ns to.tuo.b1ng0.web-ui-test
  (:require
   [cljs.test :refer-macros [deftest is testing run-tests]]
   [to.tuo.b1ng0.web-ui :as web-ui]))

(deftest user-request-message
  (is (= [:b1ng0/request-user ""]
         (web-ui/user-request-message ""))))

(deftest failing-test
  (is (= 1 1)))

(deftest wait-on-user
  (testing "waiting on user asdf"
    (is (= {:waiting-on-user "asdf"}
           (web-ui/wait-on-user {} "asdf"))))
  (testing "waiting on empty user"
    (is (= {:waiting-on-user ""}
           (web-ui/wait-on-user {} ""))))
  (testing "waiting on bobby tables"
    (is (= {:waiting-on-user "<"}
           (web-ui/wait-on-user {} "<"))))
  (testing "waiting on bobby tables' cousin"
    (is (= {:waiting-on-user "\""}
           (web-ui/wait-on-user {} "\"")))))

(deftest requested-user-textfield
  (is (= [:input#user {:type :text, :placeholder "user"}]
         (web-ui/requested-user-textfield nil))))

(deftest add-on-click
  (is (= [:br {:on-click 'mock-fn}]
         (web-ui/add-on-click [:br] 'mock-fn)))
  (is (= [:h1 {:on-click 'mock-fn} "hi"]
         (web-ui/add-on-click [:h1 "hi"] 'mock-fn))))

(deftest new-game-button
  (is (= [:button "new game"]
         (web-ui/new-game-button-static 'mock-state)))
  (is (fn? (get-in
            (web-ui/new-game-button 'mock-state)
            [1 :on-click]))))

(deftest request-user-button
  (is (= [:button#request-user-button "login"]
         (web-ui/request-user-button-static 'mock-state)))
  (is (fn? (get-in
            (web-ui/request-user-button 'mock-state)
            [1 :on-click]))))

;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with b1ng0.  If not, see <http://www.gnu.org/licenses/>.
