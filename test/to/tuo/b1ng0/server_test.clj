;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

(ns to.tuo.b1ng0.server-test
  (:require
   [clojure.test :refer [is deftest testing]]
   [hiccup.page :refer [html5 include-css include-js]]
   [to.tuo.b1ng0.server :as server]
   [clojure.test.check :as tc]
   [clojure.test.check.clojure-test :refer [defspec]]
   [clojure.test.check.generators :as gen]
   [clojure.test.check.properties :as prop]))

(def property
  (prop/for-all [v (gen/vector gen/small-integer)]
    (let [s (sort v)]
      (and (= (count v) (count s))
           (or (empty? s)
               (apply <= s))))))
(comment
  ;; test our property
  (tc/quick-check 100 property))
;; => {:result true,
;; =>  :pass? true,
;; =>  :num-tests 100,
;; =>  :time-elapsed-ms 90,
;; =>  :seed 1528578896309}
(defspec good-100
  100
  property)

(comment
  (def bad-property
    (prop/for-all [v (gen/vector gen/small-integer)]
                  (or (empty? v) (apply <= v))))

  (comment
    (tc/quick-check 100 bad-property))

  (defspec bad-100
    100
    bad-property))

(deftest debug
  (is (= "1\n"
         (with-out-str (server/debug true 1))))
  (is (= 1
         (server/debug false 1))))

;; (deftest handle-home-data
;;   (is (re-find
;;        #"BINGO"
;;        (apply str
;;               (server/handle-home-data {:anti-forgery-token 0}))))
;;   (is (re-find
;;        #"loading..."
;;        (apply str
;;               (server/handle-home-data {:anti-forgery-token 0}))))
;;   (is (re-find
;;        #"asdf.css"
;;        (apply str
;;               (server/handle-home-data
;;                {:anti-forgery-token 0}
;;                (include-css "asdf.css")
;;                (include-js "asdf.js")))))
;;   (is (re-find
;;        #"asdf.js"
;;        (apply str
;;               (server/handle-home-data
;;                {:anti-forgery-token 0}
;;                (include-css "asdf.css")
;;                (include-js "asdf.js")))))
;;   (is (re-find
;;        #"data-csrf-token 0"
;;        (apply str
;;               (server/handle-home-data {:anti-forgery-token 0})))))

(deftest handle-home
  (is (re-find #"BINGO"
               (server/handle-home {:anti-forgery-token 0})))
  (is (re-find #"loading..."
               (server/handle-home {:anti-forgery-token 0})))
  (is (re-find #"b1ng0.css"
               (server/handle-home {:anti-forgery-token 0})))
  (is (re-find #"main.js"
               (server/handle-home {:anti-forgery-token 0})))
  (is (re-find #"data-csrf-token=\"0\""
               (server/handle-home {:anti-forgery-token 0}))))

(deftest broadcast-game-message
  (testing "game state message"
    (is (= [:b1ng0/state nil]
           (server/broadcast-game-message (atom nil))))))

(deftest check-ping-response
  (testing "ping response"
    (is (= "."
           (server/ping-response nil)))))

(deftest ws-event-id->handler-name
  (testing "create potential handler name from namespaced keyword"
    (is (= "handle-a+b"
           (server/ws-event-id->handler-name :a/b))))
  (testing "create potential handler name from bare keyword"
    (is (= "handle-+b"
           (server/ws-event-id->handler-name :b))))

  ;; no error handling for now
  (comment
    (testing "refuse nil"
      (is (= nil
             (server/ws-event-id->handler-name nil))))
    (testing "refuse ids with '+' in them"
      (is (= nil
             (server/ws-event-id->handler-name :a+b))))))

(deftest ws-event-id-handled?
  (testing ":chsk/ws-ping is handled"
    (is (server/ws-event-id-handled? :chsk/ws-ping)))
  (testing "a/b is unhandled"
    (is (not (server/ws-event-id-handled? :a/b)))))



;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with b1ng0.  If not, see <http://www.gnu.org/licenses/>.
