;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

(ns to.tuo.b1ng0.model-test
  (:require
   [clojure.test :refer [is deftest testing]]
   [to.tuo.b1ng0.model :as model]))

(deftest annotate-history
  (testing "empty history leads to empty annotated history"
    (is (= []
           (model/annotate-history []))))
  (testing "history with 1 element is annotated correctly"
    (is (= [{:cell 7 :prior 0}]
           (model/annotate-history [7]))))
  (testing "history with 2 of same element is annotated correctly"
    (is (= [{:cell 7 :prior 0} {:cell 7 :prior 1}]
           (model/annotate-history [7 7]))))
  (testing "history with 2 different elements is annotated correctly"
    (is (= [{:cell 7 :prior 0} {:cell 8 :prior 0}]
           (model/annotate-history [7 8])))))

(deftest make-master-card
  (let [master-card (model/make-master-card "BINGO" 15)]
    (testing "number of elements in master card"
      (is (= (* 15 (count "BINGO"))
             (count (reduce into [] master-card)))))
    (testing "distinct elements in master card"
      (is (apply distinct? (reduce into [] master-card))))
    (testing "as many columns as there are letters in the title"
      (is (= (count "BINGO")
             (count master-card))))
    (testing "as many rows as specified on creation"
      (is (= 15
             (count (first master-card)))))))

(deftest user-card
  (let [example-card (model/make-user-card
                      (model/make-master-card "BINGO" 15))]
    (testing "user card size"
      (is (= (count (apply concat example-card))
             (* (count "BINGO") (count "BINGO")))))
    (testing "user card is a square"
      (is (= (count "BINGO")
             (count example-card)))
      (is (= (count "BINGO")
             (count (first example-card)))))
    (testing "user card values are distinct"
      (is (apply distinct? (reduce into [] example-card))))))

(deftest initial-callable-cells
  (testing "initial callable cells"
    (is (= (* (count "BINGO") 15)
           (count (model/initial-callable-cells "BINGO" 15))))))

(deftest transpose
  (let [matrix   [[1 2 3]
                  [4 5 6]
                  [7 8 9]]
        t-matrix [[1 4 7]
                  [2 5 8]
                  [3 6 9]]]
    (testing "transpose swaps column and row coordinates"
      (is (= t-matrix
             (model/transpose matrix))))))

(deftest cell->title-character
  (let [title-and-column-range {:column-range 15
                                :title        "BINGO"}]
    (is (= \B
           (model/cell->title-character title-and-column-range 1)))
    (is (= \B
           (model/cell->title-character title-and-column-range 15)))
    (is (= \I
           (model/cell->title-character title-and-column-range 16)))))

(deftest toggle
  (is (= (model/toggle #{} 1) #{1}))
  (is (= (model/toggle #{1} 1) #{})))

(deftest remove-call-from-users
  (testing "remove call from empty users"
    (is (= {:users {}}
           (model/remove-call-from-users
            {:users {}}
            1))))
  (testing "remove call when user has no calls"
    (is (= {:users {"" {:called-cells #{}}}}
           (model/remove-call-from-users
            {:users {"" {:called-cells #{}}}}
            1))))
  (testing "remove call from user who has not called cell"
    (is (= {:users {"" {:called-cells #{2}}}}
           (model/remove-call-from-users
            {:users {"" {:called-cells #{2}}}}
            1))))
  (testing "remove call from user who has called cell"
    (is (= {:users {"" {:called-cells #{}}}}
           (model/remove-call-from-users
            {:users {"" {:called-cells #{1}}}}
            1)))
    (is (= {:users {"" {:called-cells #{2}}}}
           (model/remove-call-from-users
            {:users {"" {:called-cells #{1 2}}}}
            1))))
  (testing "remove call from users who has called cell"
    (is (= {:users {"a" {:called-cells #{}}
                    "b" {:called-cells #{2}}}}
           (model/remove-call-from-users
            {:users {"a" {:called-cells #{1}}
                     "b" {:called-cells #{1 2}}}}
            1)))))

(deftest toggle-cell
  (testing "toggling an uncalled cell"
    (is (= {:call-history   [1]
            :callable-cells #{}
            :called-cells   #{1}}
           (model/toggle-cell {:call-history   []
                               :callable-cells #{1}
                               :called-cells   #{}}
                              1))))
  (testing "toggling a called cell"
    (is (= {:call-history   [1 1]
            :callable-cells #{1}
            :called-cells   #{}}
           (model/toggle-cell {:call-history   [1]
                               :callable-cells #{}
                               :called-cells   #{1}}
                              1)))))

(deftest toggle-cell!
  (testing "toggling an uncalled cell!"
    (is (= {:call-history   [1]
            :callable-cells #{}
            :called-cells   #{1}}
           (model/toggle-cell! (atom {:call-history   []
                                      :callable-cells #{1}
                                      :called-cells   #{}})
                               1))))
  (testing "toggling a called cell!"
    (is (= {:call-history   [1 1]
            :callable-cells #{1}
            :called-cells   #{}}
           (model/toggle-cell! (atom {:call-history   [1]
                                      :callable-cells #{}
                                      :called-cells   #{1}})
                               1)))))

(deftest call-cell
  (testing "call a callable cell"
    (is (= {:call-history   [1]
            :callable-cells #{}
            :called-cells   #{1}}
           (model/call-cell {:call-history   []
                         :callable-cells #{1}
                         :called-cells   #{}}
                        1)))))

(deftest user-call-cell
  (testing "user marks cell that master called"
    (is (= {:called-cells #{1}
            :users        {"a" {:called-cells #{1}}}}
           (model/user-call-cell
            {:called-cells #{1}
             :users        {"a" {:called-cells #{}}}}
            {:user "a"
             :cell 1}))))
  (testing "user does not mark cell that master did not called"
    (is (= {:called-cells #{}
            :users        {"a" {:called-cells #{}}}}
           (model/user-call-cell
            {:called-cells #{}
             :users        {"a" {:called-cells #{}}}}
            {:user "a"
             :cell 1})))))

(deftest user-call-cell!
  (testing "user marks cell that master called!"
    (is (= {:called-cells #{1}
            :users        {"a" {:called-cells #{1}}}}
           (model/user-call-cell!
            (atom
             {:called-cells #{1}
              :users        {"a" {:called-cells #{}}}})
            {:user "a"
             :cell 1}))))
  (testing "user does not mark cell that master did not called!"
    (is (= {:called-cells #{}
            :users        {"a" {:called-cells #{}}}}
           (model/user-call-cell!
            (atom
             {:called-cells #{}
              :users        {"a" {:called-cells #{}}}})
            {:user "a"
             :cell 1})))))

(deftest random-callable-cell
  (let [callable-cells (set (range 1 (inc (* (count "BINGO") 15))))]
    (testing "randomly called cell is a member of callable cells"
      (is (callable-cells
           (model/random-callable-cell
            {:callable-cells callable-cells}))))))

(deftest call-random-cell
  (testing "call random cell when there are no callable cells"
    (is (= {:callable-cells #{}}
           (model/call-random-cell {:callable-cells #{}}))))
  (testing "call random cell when there is only one callable cell"
    (is (= {:called-cells   #{1}
            :callable-cells #{}
            :call-history   [1]}
           (model/call-random-cell {:callable-cells #{1}
                                    :called-cells   #{}
                                    :call-history   []})))))

(deftest call-random-cell!
  (testing "call random cell when there are no callable cells!"
    (is (= {:callable-cells #{}}
           (model/call-random-cell! (atom {:callable-cells #{}})))))
  (testing "call random cell when there is only one callable cell!"
    (is (= {:called-cells   #{1}
            :callable-cells #{}
            :call-history   [1]}
           (model/call-random-cell! (atom {:callable-cells #{1}
                                           :called-cells   #{}
                                           :call-history   []}))))))

(deftest get-most-recent-called-cell
  (testing "get most recent from empty call history"
    (is (= nil
           (model/get-most-recent-called-cell
            (atom {:call-history [] :called-cells #{}})))))
  (testing "get most recent from single element call history"
    (is (= 1
           (model/get-most-recent-called-cell
            (atom {:call-history [1] :called-cells #{1}})))))
  (testing "get most recent from called and uncalled cell"
    (is (= nil
           (model/get-most-recent-called-cell
            (atom {:call-history [1 1] :called-cells #{}})))))
  (testing "get most recent from called, uncalled, recalled cell"
    (is (= 1
           (model/get-most-recent-called-cell
            (atom {:call-history [1 1 1] :called-cells #{1}}))))))

(deftest called-cell?
  (testing "uncalled cell from empty called cells"
    (is (not (model/called-cell? {:called-cells #{}} 1))))
  (testing "uncalled cell from non-empty called cells"
    (is (not (model/called-cell? {:called-cells #{2}} 1))))
  (testing "called cell from non-empty called cells"
    (is (model/called-cell? {:called-cells #{1}} 1))))

(deftest diagonals
  (let [test-matrix [[ 1  2  3  4  5]
                     [16 17 18 19 20]
                     [31 32 33 34 35]
                     [46 47 48 49 50]
                     [61 62 63 64 65]]]
    (is (= 2
           (count (model/diagonals test-matrix))))
    (is (= #{#{1 17 33 49 65} #{61 47 33 19 5}}
           (into #{} (map set (model/diagonals test-matrix)))))))

(deftest user-called?
  (testing "whether user called cell when user has not called anything"
    (is (not (model/user-called? {:user  "a"
                                  :users {"a" {:called-cells #{}}}}
                                 1))))
  (testing "whether user called cell when user has not called that cell"
    (is (not (model/user-called? {:user  "a"
                                  :users {"a" {:called-cells #{2}}}}
                                 1))))
  (testing "whether user called cell when user has called that cell"
    (is (model/user-called? {:user  "a"
                             :users {"a" {:called-cells #{1}}}}
                            1))))

(deftest clear-user-called-cells
  (testing "clear an already empty user called cells"
    (is (= {:user "a" :users {"a" {:called-cells #{}}}}
           (model/clear-user-called-cells
            {:user "a" :users {"a" {:called-cells #{}}}} "a"))))
  (testing "clear non-empty user called cells"
    (is (= {:user "a" :users {"a" {:called-cells #{}}}}
           (model/clear-user-called-cells
            {:user "a" :users {"a" {:called-cells #{1}}}} "a")))))

(deftest clear-all-users-called-cells
  (testing
      "clearing all users called cells with no users or called cells"
    (is (= {:users {}}
           (model/clear-all-users-called-cells {:users {}}))))
  (testing
      "clearing all users called cells with a user and no called cells"
    (is (= {:users {}}
           (model/clear-all-users-called-cells {:users {}}))))
  (testing
      "clearing all users called cells with a user and called cells"
    (is (= {:users {"a" {:called-cells #{}}}}
           (model/clear-all-users-called-cells
            {:users {"a" {:called-cells #{1}}}}))))
  (testing
      "clearing all users called cells with 2 users and called cells"
    (is (= {:users {"a" {:called-cells #{}}
                    "b" {:called-cells #{}}}}
           (model/clear-all-users-called-cells
            {:users {"a" {:called-cells #{1}}
                     "b" {:called-cells #{1}}}})))))

(deftest clear-call-history
  (testing "clearing an empty master call history"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)}
           (model/clear-call-history
            {:call-history   []
             :called-cells   #{}
             :callable-cells (model/initial-callable-cells
                              "BINGO" 15)}))))
  (testing "clearing a non-empty master call history"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)}
           (model/clear-call-history
            {:call-history   [1]
             :called-cells   #{1}
             :callable-cells #{}})))))

(deftest new-game
  (testing "creating a new game from a brand new game"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {}}
           (model/new-game
            {:call-history []
             :called-cells #{}
             :users        {}

             :callable-cells
             (model/initial-callable-cells "BINGO" 15)}))))
  (testing "creating a new game from a game with a called cell"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {}}
           (model/new-game
            {:call-history [1]
             :called-cells #{1}
             :users        {}

             :callable-cells
             (model/initial-callable-cells "BINGO" 15)}))))
  (testing "master called cell and user -> new game"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {"a" {:called-cells #{}}}}
           (model/new-game
            {:call-history [1]
             :called-cells #{1}
             :users        {"a" {:called-cells #{}}}

             :callable-cells
             (model/initial-callable-cells "BINGO" 15)}))))
  (testing "master called cell and user with called cell -> new game"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {"a" {:called-cells #{}}}}
           (model/new-game
            {:call-history [1]
             :called-cells #{1}
             :users        {"a" {:called-cells #{1}}}

             :callable-cells
             (model/initial-callable-cells "BINGO" 15)})))))

(deftest new-game!
  (testing "creating a new game from a brand new game!"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {}}
           (model/new-game!
            (atom
             {:call-history []
              :called-cells #{}
              :users        {}

              :callable-cells
              (model/initial-callable-cells "BINGO" 15)})))))
  (testing "creating a new game from a game with a called cell!"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {}}
           (model/new-game!
            (atom
             {:call-history [1]
              :called-cells #{1}
              :users        {}

              :callable-cells
              (model/initial-callable-cells "BINGO" 15)})))))
  (testing "master called cell and user -> new game!"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {"a" {:called-cells #{}}}}
           (model/new-game!
            (atom
             {:call-history [1]
              :called-cells #{1}
              :users        {"a" {:called-cells #{}}}

              :callable-cells
              (model/initial-callable-cells "BINGO" 15)})))))
  (testing "master called cell and user with called cell -> new game!"
    (is (= {:call-history   []
            :called-cells   #{}
            :callable-cells (model/initial-callable-cells "BINGO" 15)
            :users          {"a" {:called-cells #{}}}}
           (model/new-game!
            (atom
             {:call-history [1]
              :called-cells #{1}
              :users        {"a" {:called-cells #{1}}}

              :callable-cells
              (model/initial-callable-cells "BINGO" 15)}))))))

(deftest make-user
  (testing "created user has correct name"
    (is (= "a"
           (:user (model/make-user
                   (atom {:master-card (model/make-master-card
                                        "BINGO" 15)})
                   "a")))))
  (testing "created user has card"
    (is (:card (model/make-user
                (atom {:master-card (model/make-master-card
                                     "BINGO" 15)})
                "a"))))
  (testing "created user has empty called cells"
    (is (empty?
         (:called-cells
          (model/make-user (atom {:master-card (model/make-master-card
                                                "BINGO" 15)})
                           "a"))))))

(deftest create-state! []
  (is (= "BINGO"
         (:title @(model/create-state!)))))

(deftest request-user!
  (let [state     (model/create-state!)
        with-asdf (model/request-user! state "asdf")]
    (testing "create user with correct name"
      (is (= "asdf"
             (get-in with-asdf [:users "asdf" :user])))))
  (let [state     (model/create-state!)
        _a        (model/request-user! state "asdf")
        users     (get _a :users)
        _b        (model/request-user! state "asdf")
        new-users (get _b :users)]
    (testing "leave extant user alone"
      (is (= users new-users)))))

;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with b1ng0.  If not, see <http://www.gnu.org/licenses/>.
