#! /usr/bin/env bash

# GNU AGPLv3 (or later at your option) see bottom for more license
# info.  please keep this notice.

INKSCAPE="flatpak run org.inkscape.Inkscape"

sizes=(16 32 64 128 256)
for size in ${sizes[@]}
do
    $INKSCAPE \
	--export-area=0:0:160:160 \
	--export-width $size \
	--export-height $size \
	--export-filename $size.png \
	favicon.svg
done

convert 16.png 32.png 64.png 128.png 256.png favicon.ico

pictures=( "${sizes[@]/%/.png}" )
rm ${pictures[@]}

# This file is part of b1ng0.

# b1ng0 is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.

# b1ng0 is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with b1ng0.  If not, see
# <http://www.gnu.org/licenses/>.
